# Jockpass

Sometimes simple guessing in brute forcing makes close guessings but the target is slightly different. The idea of this tool is to poke around incorrect guessed passwords in case the target is similar but not exactly that one. 

It uses a **simple genetic algorithm** and saves the guessings of a **desired fitness** in an output file ready to be used in your favorite bruteforcing tool. 

![](https://media.giphy.com/media/20HHkJ3UpFZCt4nQwZ/giphy.gif)

## Compile and use:

```
$ g++ jockpass.c -o jockpass
$ ./jockpass {pass} {fitness}
```

For example:

```
$ ./guess PaS5w0Rd 1
$ cat candidates.txt
```
Would show something like this:

```
Generation: 128	String: PaS5w0Rd	Fitness: 0
...
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq
PaS5w0Rq

```
In case you have repeated options (like the one above) you can use the `clean_file.sh` script to remove duplicates with awk.

```
$ ./clean_file.sh
```
This has a [GNU](https://gitlab.com/terceranexus6/jock_pass/-/blob/master/LICENSE) licence.

Things I have in mind to enhance the tool:
* Pass a list of passwords instead of just one
* Better algorithm?
* Other languages

